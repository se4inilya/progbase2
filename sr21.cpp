#include <string>
#include <vector>
#include <iostream>
#include <ctype.h>
#include <algorithm>

using namespace std;

int main()
{
    string str = "Hello0QueuE13Lviv112Pop94DOG11Love24Age";
    vector <string> str_vector;
    vector <int> int_vector;
    char buf[50];
    int bufX = 0;
    std::cout << "start" << endl;
    for (string::iterator iter = str.begin(); iter != str.end(); iter += 1)
    {
        if (isalpha(*iter))
        {
            while (isalpha(*iter))
            {
                if (*iter >= 65 && *iter <= 97)
                {
                    *iter += 32;
                }
                buf[bufX++] = *iter;
                iter += 1;
            }
            buf[bufX] = '\0';
            bufX = 0;
            iter -= 1;
            str_vector.push_back(buf);
        }
        else if (isdigit(*iter))
        {
            while (isdigit(*iter))
            {
                buf[bufX++] = *iter;
                iter += 1;
            }
            buf[bufX] = '\0';
            bufX = 0;
            iter -= 1;
            int_vector.push_back(atoi(buf));
        }
    }

    sort(int_vector.begin(), int_vector.end());
    sort(str_vector.begin(), str_vector.end());

    for (int i = 0; i < str_vector.size(); i++)
    {
        std::cout << "str_vector: " << str_vector.at(i) << endl;
    }
    for (int i = 0; i < int_vector.size(); i++)
    {
        std::cout << "int_vector: " << int_vector.at(i) << endl;
    }


    return 0;
}