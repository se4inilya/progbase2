#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include <algorithm>

using namespace std;

int main(){
    string str;
    cin >> str;

    stringstream buf(str);
    string temp_str;
    vector<string> vec;

    while(getline(buf, temp_str, ',')){
        vec.push_back(temp_str);
        temp_str.clear();
    }
    
    sort(vec.begin(), vec.end());
    
    for(int i = 0; i < vec.size(); i++){
        string s = vec.at(i);
        cout << s << endl;
    }
    
    return 0;
}