#include <string>
#include <sstream>
#include <iostream>
#include <ctype.h>
#include <algorithm>
#include "sr2.h"
using namespace std;

int main()
{
    string str = " 10, wea 07  07 0xff 0xfff 0xwwq";
    vector<Sr2::Number> vec = Sr2::getNumbers(str);
    for (int i = 0; i < vec.size(); i++)
    {
        cout << vec.at(i).base << endl;
        cout << vec.at(i).number << endl;
    }
}

Sr2::Number::Number(const string &number, NumberBase base)
{
    Sr2::Number::number = number;
    Sr2::Number::base = base;
}

Sr2::Number::Number(const string &number)
{
    Sr2::Number::number = number;
}
vector<Sr2::Number> Sr2::getNumbers(const string &text)
{
    vector<Sr2::Number> numbers;
    //vector <string> str_vector;
    string str = text;
    char buf[50];
    int bufX = 0;
    for (string::iterator iter = str.begin(); iter != str.end(); iter += 1)
    {
        Sr2::NumberBase en = Decimal;
        if (isdigit(*iter))
        {
            if (*iter == '0')
            {
                if (isalpha(*(iter + 1)))
                {
                    en = Hex;
                    while (isdigit(*iter) || isalpha(*iter))
                    {
                        buf[bufX++] = *iter;
                        iter += 1;
                    }
                }
                else
                {
                    en = Octal;
                    while (isdigit(*iter))
                    {
                        buf[bufX++] = *iter;
                        iter += 1;
                    }
                }
            }
            else
            {
                //en = Decimal;
                while (isdigit(*iter))
                {
                    buf[bufX++] = *iter;
                    iter += 1;
                }
            }
            buf[bufX] = '\0';
            bufX = 0;
            iter -= 1;
            int f = 0;
            for (int i = 0; i < numbers.size(); i++)
            {
                if (buf == numbers.at(i).number)
                {
                    f = 1;
                }
            }
            if (f == 0)
            {
                struct Sr2::Number num(buf, en);

                numbers.push_back(num);
            }
        }
    }

    return numbers;
}