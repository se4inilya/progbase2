#ifndef SQLITE_STORAGE_H
#define SQLITE_STORAGE_H

#include "storage.h"
#include <QSqlDatabase>

class SqliteStorage : public Storage
{
    const string dir_name_;
    QSqlDatabase db_;

public:
    SqliteStorage(const string & dir_name);

    bool open();
    bool close();
    // books
    vector<Book> getAllBooks(void);
    optional<Book> getBookById(int book_id);
    bool updateBook(const Book &book);
    bool removeBook(int book_id);
    int insertBook(const Book &book);
    // Authors
    vector<Author> getAllAuthors(void);
    optional<Author> getAuthorById(int author_id);
    bool updateAuthor(const Author &author);
    bool removeAuthor(int author_id);
    int insertAuthor(const Author &author);
};

#endif // SQLITE_STORAGE_H
