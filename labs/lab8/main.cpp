#include "csv_storage.h"
#include "xml_storage.h"
#include "sqlite_storage.h"
#include "cui.h"
#include <iostream>
#include <QFileInfo>

using namespace std;

Storage * createStorage(const QString & qpath);

int main()
{
    Storage * storage_ptr = createStorage("../lab8/data/sql");
    storage_ptr->open();

    Cui cui(storage_ptr);
    cui.show();

    storage_ptr->close();

    delete storage_ptr;
    return 0;
}

Storage * createStorage(const QString & qpath){
    if(qpath.endsWith("csv")){
        CsvStorage * storage = new CsvStorage(qpath.toLocal8Bit().constData());
        return storage;
    }
    else if(qpath.endsWith("xml")){
        XmlStorage * storage = new XmlStorage(qpath.toLocal8Bit().constData());
        return storage;
    }
    else if(qpath.endsWith("sql")){
        SqliteStorage * storage = new SqliteStorage(qpath.toLocal8Bit().constData());
        return storage;
    }
    else{
        qDebug() << "Can't find any files for storage";
        abort();
    }
}
