#pragma once

#include <vector>
#include <string>

#include "optional.h"
#include "book.h"
#include "author.h"
#include "csv.h"

using std::string;
using std::vector;

class Storage
{
 public:
    virtual ~Storage(){}

   virtual bool open() = 0;
   virtual bool close() = 0;
   // books
   virtual vector<Book> getAllBooks(void) = 0;
   virtual optional<Book> getBookById(int book_id) = 0;
   virtual bool updateBook(const Book &book) = 0;
   virtual bool removeBook(int book_id) = 0;
   virtual int insertBook(const Book &book) = 0;
   // Authors
   virtual vector<Author> getAllAuthors(void) = 0;
   virtual optional<Author> getAuthorById(int author_id) = 0;
   virtual bool updateAuthor(const Author &author) = 0;
   virtual bool removeAuthor(int author_id) = 0;
   virtual int insertAuthor(const Author &author) = 0;
};
