#pragma once
#include <string>

struct Book
{
    int id;
    std::string book_name;
    std::string author_name;
    int character_capacity;
};

