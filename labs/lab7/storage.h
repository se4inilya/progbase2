#pragma once

#include <vector>
#include <string>

#include "optional.h"
#include "book.h"
#include "csv.h"

using std::string;
using std::vector;

class Storage
{
 public:
   virtual ~Storage() = default;

   virtual bool load() = 0;
   virtual bool save() = 0;
   // books
   virtual vector<Book> getAllBooks(void) = 0;
   virtual optional<Book> getBookById(int book_id) = 0;
   virtual bool updateBook(const Book &book) = 0;
   virtual bool removeBook(int book_id) = 0;
   virtual int insertBook(const Book &book) = 0;
};
