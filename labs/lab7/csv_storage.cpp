#include "csv_storage.h"
#include <iostream>
#include <fstream>

using namespace std;

Book CsvStorage::rowToBook(const CsvRow &row)
{
    Book book;
    book.id = stoi(row.at(0));
    book.book_name = row.at(1);
    book.author_name = row.at(2);
    book.character_capacity = stoi(row.at(3));

    return book;
}
CsvRow CsvStorage::bookToRow(const Book &st)
{
    CsvRow row;
    row.push_back(std::to_string(st.id));
    row.push_back(st.book_name);
    row.push_back(st.author_name);
    row.push_back(std::to_string(st.character_capacity));
    return row;
}

bool CsvStorage::load()
{
    string books_filename = this->dir_name_;
    ifstream infile;
    infile.open(books_filename);
    if (!infile.good())
    {
        cerr << "Can't open " << books_filename;
        abort();
    }
    string row_str;
    string text_str;
    while (getline(infile, row_str))
    {
        text_str += row_str + "\n";
    }

    CsvTable books_table = Csv::createTableFromString(text_str);

    for (CsvRow &row : books_table)
    {
        Book b = CsvStorage::rowToBook(row);
        this->books_.push_back(b);
    }

    infile.close();
    return true;
}
bool CsvStorage::save()
{
    string books_filename = this->dir_name_;
    ofstream outfile;
    outfile.open(books_filename);
    if (!outfile.good())
    {
        cerr << "Can't open " << books_filename;
        abort();
    }
    CsvTable table;
    for (Book &b : this->books_)
    {
        CsvRow row = CsvStorage::bookToRow(b);
        table.push_back(row);
    }
    string buf = Csv::createStringFromTable(table);
    outfile << buf;
    outfile.close();
    
    return true;
}

vector<Book> CsvStorage::getAllBooks(void)
{
    return this->books_;
}
optional<Book> CsvStorage::getBookById(int book_id)
{
    for (Book &b : this->books_)
    {
        if (b.id == book_id)
        {
            return b;
        }
    }
    return nullopt;
}
bool CsvStorage::updateBook(const Book &book)
{
    int size = this->books_.size();
    for (int i = 0; i < size; i++)
    {
        if (this->books_[i].id == book.id)
        {
            this->books_[i].book_name = book.book_name;
            this->books_[i].author_name = book.author_name;
            this->books_[i].character_capacity = book.character_capacity;
            return true;
        }
    }
    return false;
}
bool CsvStorage::removeBook(int book_id)
{
    int index = -1;
    int size = this->books_.size();
    for (int i = 0; i < size; i++)
    {
        if (this->books_[i].id == book_id)
        {
            index = i;
            break;
        }
    }
    if (index >= 0)
    {
        this->books_.erase(this->books_.begin() + index);
        return true;
    }
    return false;
}
int CsvStorage::insertBook(const Book &book)
{
    Book copy = book;
    copy.id = this->getNewBookId();
    this->books_.push_back(copy);
    return copy.id;
}

int CsvStorage::getNewBookId()
{
    int max = 0;
    for (Book &b : this->books_)
    {
        if (max < b.id)
        {
            max = b.id;
        }
    }
    max++;
    return max;
}
