#include "adddialog.h"
#include "ui_adddialog.h"
#include "mainwindow.h"

AddDialog::AddDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddDialog)
{
    ui->setupUi(this);
}

AddDialog::~AddDialog()
{
    delete ui;
}



void AddDialog::on_ok_button_clicked()
{
    Book * book = new Book();

    book->book_name = ui->lineEdit->text().toStdString();
    book->author_name = ui->lineEdit_2->text().toStdString();
    book->character_capacity = ui->spinBox->value();

    emit sendSignal(book);
    AddDialog::close();
}

void AddDialog::on_pushButton_2_clicked()
{
    AddDialog::close();
}
