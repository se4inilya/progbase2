#pragma once

#include <vector>
#include <string>

#include "storage.h"
#include "optional.h"
#include "book.h"
#include "csv.h"

using std::string;
using std::vector;

class CsvStorage : public Storage
{
   const string dir_name_;

   vector<Book> books_;

   static Book rowToBook(const CsvRow &row);
   static CsvRow bookToRow(const Book &st);

   int getNewBookId();

 public:
   CsvStorage(const string & dir_name) : dir_name_(dir_name) { }

   bool load();
   bool save();
   // books
   vector<Book> getAllBooks(void);
   optional<Book> getBookById(int book_id);
   bool updateBook(const Book &book);
   bool removeBook(int book_id);
   int insertBook(const Book &book);
};

