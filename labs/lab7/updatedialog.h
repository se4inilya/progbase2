#ifndef UPDATEDIALOG_H
#define UPDATEDIALOG_H

#include <QDialog>
#include <QListWidget>
#include "book.h"

namespace Ui {
class UpdateDialog;
}

class UpdateDialog : public QDialog
{
    Q_OBJECT

public:
    explicit UpdateDialog(QWidget *parent = 0);
    ~UpdateDialog();

signals:
    void sendUpdatedSignal(Book *);

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void receiveBookToEdit(QListWidgetItem *);

private:
    Ui::UpdateDialog *ui;
};

#endif // UPDATEDIALOG_H
