#include "updatedialog.h"
#include "ui_updatedialog.h"

#include "mainwindow.h"

UpdateDialog::UpdateDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UpdateDialog)
{
    ui->setupUi(this);
}

UpdateDialog::~UpdateDialog()
{
    delete ui;
}

void UpdateDialog::on_pushButton_clicked()  //OK
{
    Book* book = new Book();

    book->book_name = ui->lineEdit->text().toStdString();
    book->author_name = ui->lineEdit_2->text().toStdString();
    book->character_capacity = ui->spinBox->value();


    emit(sendUpdatedSignal(book));

    UpdateDialog::close();
}


void UpdateDialog::on_pushButton_2_clicked()  //Cancel
{
    UpdateDialog::close();
}

void UpdateDialog::receiveBookToEdit(QListWidgetItem * item){
    QVariant qvar = item->data(Qt::UserRole);
    Book book = qvar.value<Book>();

    ui->lineEdit->setText(QString::fromStdString(book.book_name));
    ui->lineEdit_2->setText(QString::fromStdString(book.author_name));
    ui->spinBox->setValue(book.character_capacity);
}
