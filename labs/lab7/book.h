#pragma once
#include <string>
#include <qmetatype.h>

struct Book
{
    int id;
    std::string book_name;
    std::string author_name;
    int character_capacity;
};

Q_DECLARE_METATYPE(Book)
