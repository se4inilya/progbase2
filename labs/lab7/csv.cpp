#include "csv.h"
#include <sstream>
#include <iostream>

using namespace std;

CsvTable Csv::createTableFromString(const string &csv_str)
{
    CsvTable table;
    stringstream buf(csv_str);
    string buf1;
    string rowbuff;
    CsvRow row;
    while(getline(buf, buf1, '\n')){
        stringstream rowstream(buf1);
        while(getline(rowstream, rowbuff, ',')){
            row.push_back(rowbuff);
            rowbuff.clear();
        }
        table.push_back(row);
        row.clear();
        buf1.clear();
    }

    return table;
}


string Csv::createStringFromTable(const CsvTable &csv_table)
{
    string str;

    int size_of_the_table = csv_table.size();
    
    for(int i = 0; i < size_of_the_table; i++){
        int current_size = csv_table[i].size();
        for(int j = 0; j < current_size; j++){
            str += csv_table[i][j];
            if(!(j == current_size - 1)) str += ",";
                        
        }
        str += "\n";
    }
    return str;
}

