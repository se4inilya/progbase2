#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "adddialog.h"

#include <QDebug>
#include <QFileDialog>
#include <QDialog>
#include <QMessageBox>

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->create_button->setEnabled(false);
    ui->delete_button->setEnabled(false);
    ui->update_button->setEnabled(false);
}

MainWindow::~MainWindow()
{
    delete storage_;
    delete ui;
}

void MainWindow::on_actionOpen_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(
                   this,              // parent
                   "Open file",  // caption
                   "/home/iluha/projects/progbase2/labs/lab7/data/",                // directory to start with
                   "CSV (*.csv);;All Files (*)");  // file name filter
       qDebug() << fileName;
       std::string name = fileName.toStdString();
       const char * namec = name.c_str();
       char c_name[100];
       int num_of_slash = 0;
       for (; *namec != '\0'; namec += 1)
       {
           if (*namec == '/')
               num_of_slash += 1;
       }
       namec = name.c_str();
       for (int i = 0;; i += 1)
       {
           c_name[i] = *namec;
           if (*namec == '/')
               num_of_slash -= 1;
           if (num_of_slash == 0)
           {
               c_name[i + 1] = '\0';
               break;
           }
           namec += 1;
       }
       fileName = c_name;
       if (fileName.size() == 0)
           return;

       qDebug() << "Path:" << fileName;
       QDir dir(fileName);
       if (!dir.exists())
       {
           qDebug() << "Wrong path:" << fileName;
           dir.mkpath(".");
           fileName = fileName + "new_storage.csv";
           ofstream stor;
           stor.open(fileName.toStdString(), ios::out);
           stor.close();
       }
       else
           fileName = QString::fromStdString(name);

       if (storage_ != nullptr)
      {
           delete storage_;
           storage_ = nullptr;
          ui->listWidget->clear();
       }

       file_name = fileName;
       storage_ = new CsvStorage(name);

       if(!storage_->load())
       {
           //cerr << "Can't load storage: " << fileName;
           //cerr << "This file doesn't exist" << endl;
           return;
       }

       vector<Book> books = storage_->getAllBooks();

       for(int i = 0; i < books.size(); i++){
           QListWidget *listWidget = ui->listWidget;
           QListWidgetItem *bookItem = new QListWidgetItem;

           QVariant qvar;
           qvar.setValue(books.at(i));
           QString name = QString::fromStdString(books.at(i).book_name);
           bookItem->setText(name);
           bookItem->setData(Qt::UserRole, qvar);

           listWidget->addItem(bookItem);
        }

     ui->create_button->setEnabled(true);
     ui->delete_button->setEnabled(false);
     ui->update_button->setEnabled(false);
}

void MainWindow::on_actionCreate_triggered()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::Directory);
    QString current_folder = QDir::currentPath();
    QString file_path = dialog.getSaveFileName(
                this,
                "Select folder for create",
                current_folder + "/new_storage.csv",
                "Folders"
                );
    if (file_path.size() == 0)
        return;

    if (storage_ != nullptr)
    {
        delete storage_;
        storage_ = nullptr;

        ui->listWidget->clear();

        ui->update_button->setEnabled(false);
        ui->delete_button->setEnabled(false);
        ui->id_label->setText("-");
        ui->book_label->setText("-");
        ui->author_label->setText("-");
        ui->char_label->setText("-");
    }
    file_name = file_path;

    storage_ = new CsvStorage(file_name.toStdString());

    ui->create_button->setEnabled(true);

    storage_->save();
}



void MainWindow::on_listWidget_itemClicked(QListWidgetItem *book)
{
    ui->delete_button->setEnabled(true);
    ui->update_button->setEnabled(true);

    QVariant variant = book->data(Qt::UserRole);
    Book b = variant.value<Book>();

    string id = to_string(b.id);
    string char_cap = to_string(b.character_capacity);
    ui->id_label->setText(QString::fromStdString(id));
    ui->book_label->setText(QString::fromStdString(b.book_name));
    ui->author_label->setText(QString::fromStdString(b.author_name));
    ui->char_label->setText(QString::fromStdString(char_cap));
}



void MainWindow::on_delete_button_clicked()
{
    vector<Book> books = storage_->getAllBooks();
    if(books.empty()){
        return;
    }

    QMessageBox::StandardButton approve = QMessageBox::question(
            this,
            "Delete window",
            "Do you really want to delete this element ?",
            QMessageBox::Yes| QMessageBox::No
    );
    if (approve == QMessageBox::Yes) {
           qDebug() << "Yes was clicked";
       } else {
           qDebug() << "Yes was *not* clicked";
           return;
       }

    QList<QListWidgetItem *> items = ui->listWidget->selectedItems();

    if(items.at(0) == NULL){
        return;
    }

    foreach(QListWidgetItem * item, items){
        items = ui->listWidget->selectedItems();
        QVariant var = item->data(Qt::UserRole);
        Book b = var.value<Book>();

        delete ui->listWidget->takeItem(ui->listWidget->row(item));

        if(!storage_->removeBook(b.id))
            qDebug() << "Was not deleted" << endl;
    }

    storage_->save();

    books = storage_->getAllBooks();

        if(books.empty()){
            ui->delete_button->setEnabled(false);
            ui->update_button->setEnabled(false);

            ui->id_label->setText("-");
            ui->book_label->setText("-");
            ui->author_label->setText("-");
            ui->char_label->setText("-");

            return;
        }

        items = ui->listWidget->selectedItems();
        QListWidgetItem * book = items.at(0);

        QVariant variant = book->data(Qt::UserRole);
        Book b1 = variant.value<Book>();

        string id = to_string(b1.id);
        string char_cap = to_string(b1.character_capacity);
        ui->id_label->setText(QString::fromStdString(id));
        ui->book_label->setText(QString::fromStdString(b1.book_name));
        ui->author_label->setText(QString::fromStdString(b1.author_name));
        ui->char_label->setText(QString::fromStdString(char_cap));
}

void MainWindow::on_create_button_clicked()
{
    add_dialog = new AddDialog(this);
    add_dialog->show();

    connect(add_dialog, SIGNAL(sendSignal(Book*)), this, SLOT(receive_Book(Book*)));
}

void MainWindow::on_update_button_clicked()
{
    update_dialog = new UpdateDialog(this);
    update_dialog->show();

    connect(this, SIGNAL(sendSignalForEdition(QListWidgetItem*)), update_dialog, SLOT(receiveBookToEdit(QListWidgetItem*)));
    connect(update_dialog, SIGNAL(sendUpdatedSignal(Book*)), this, SLOT(receive_Updated_Book(Book*)));

    QListWidgetItem * item = ui->listWidget->selectedItems().at(0);
    emit(sendSignalForEdition(item));
}

void MainWindow::receive_Book(Book *book){
    storage_->insertBook(*book);
    vector<Book> books = storage_->getAllBooks();

    QListWidget * listw = ui->listWidget;
    QListWidgetItem *BookItem = new QListWidgetItem();

    QVariant qvar;
    qvar.setValue(books.at(books.size() - 1));

    QString name = QString::fromStdString(books.at(books.size() - 1).book_name);
    BookItem->setText(name);
    BookItem->setData(Qt::UserRole, qvar);

    listw->addItem(BookItem);
    storage_->save();
}

void MainWindow::receive_Updated_Book(Book *book){
    QList<QListWidgetItem *> items = ui->listWidget->selectedItems();
    QListWidgetItem * item = items.at(0);
    QVariant qvar = item->data(Qt::UserRole);
    Book b = qvar.value<Book>();

    book->id = b.id;

    storage_->updateBook(*book);
    storage_->save();

    QListWidgetItem * book_item = ui->listWidget->takeItem(ui->listWidget->row(item));

    QVariant var;
    var.setValue(*book);

    QString name = QString::fromStdString(book->book_name);
    book_item->setText(name);
    book_item->setData(Qt::UserRole, var);

    ui->listWidget->addItem(book_item);
}
