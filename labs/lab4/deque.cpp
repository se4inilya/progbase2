#include <iostream>
#include <cstdio>
#include <cstdlib>

#include "deque.h"

using namespace std;
Deque::Deque()
{
    this->capacity_ = 4;
    this->first_ = 0;
    this->last_ = 0;
    this->size_ = 0;
    this->items_ = static_cast<float *>(malloc(sizeof(float) * this->capacity_));
}

Deque::~Deque()
{
    free(this->items_);
}

void Deque::memAlloc(int newcapacity_){
    float *newitems_ = static_cast<float *>(realloc(this->items_, sizeof(float) * this->capacity_));
            if (newitems_ == nullptr)
            {
                fprintf(stderr, "\nReallocating error\n");
                free(this->items_);
                free(this);
                exit(EXIT_FAILURE);
            }
            this->items_ = newitems_;
}

void Deque::pushFront(float value)
{
    if (this->size_ == 0)
    {
        this->items_[this->first_] = value;
    }
    else
    {
        if (this->last_ + 1 == this->capacity_)
        {
            int newcap = this->capacity_ *= 2;
            this->memAlloc(newcap);
        }
        move(1);
        this->items_[this->first_] = value;
    }
    this->size_++;
}
void Deque::pushBack(float value)
{
    if (this->size_ == 0)
    {
        this->items_[this->first_] = value;
    }
    else
    {
        if (this->last_ + 1 == this->capacity_)
        {
            int newcap = this->capacity_ *= 2;
            this->memAlloc(newcap);
            
        }
        this->last_++;
        this->items_[this->last_] = value;
    }
    this->size_++;
}
void Deque::popFront()
{
    if (this->size_ == 0)
    {
        return;
    }
    move(-1);
    this->size_--;
}
void Deque::popBack()
{
    if (this->size_ == 0)
    {
        return;
    }
    this->last_--;
    this->size_--;
}
void Deque::print()
{
    for (int i = 0; i <= this->last_; i++)
    {
        cout << this->items_[i] << " ";
    }
    cout << endl;
}



void Deque::move(int x)
{
    if (this->size_ == 0)
    {
        return;
    }
    if (x < 0)
    {
        for (int i = 1; i <= this->last_; i++)
        {
            this->items_[i + x] = this->items_[i];
        }
        this->last_--;
        return;
    }
    if (this->last_ + x >= this->capacity_)
    {
        return;
    }
    for (int i = this->last_; i >= 0; i--)
    {
        this->items_[i + x] = this->items_[i];
    }
    this->last_ = this->last_ + x;
}

int Deque::size(){
    return this->size_;
}



float Deque::getFromFront(){
    return this->items_[this->first_];
}

float Deque::getFromEnd(){
    return this->items_[this->last_];
}