#include <cstdlib>

#include "list.h"

using namespace std;

List::List()
{
    this->cap_ = 4;
    this->arr_ = static_cast<float *>(malloc(sizeof(float) * this->cap_));
    this->size_ = 0;
}
List::~List()
{
    free(this->arr_);
}

float & List::operator [](int index){
    float & f = this->arr_[index];
    return f;
}

void List::memAlloc(int newcap_){
    float *newarr_ = static_cast<float *>(realloc(this->arr_, sizeof(float) * newcap_));
        if (newarr_ == nullptr)
        {
            free(this->arr_);
            fprintf(stderr, "Reallocating memory error\n");
            abort();
        }
        this->arr_ = newarr_;
        this->cap_ = newcap_;
}

void List::add(float newValue)
{
    (*this)[this->size_] = newValue;
    this->size_ += 1;
    // realloc mem
    if (this->size_ == this->cap_)
    {
        int newcap = this->cap_ * 2;
        this->memAlloc(newcap);
    }
}
void List::print()
{
    for (int j = 0; j < this->size_; j++)
    {
        cout << (*this)[j] << " ";
    }
    cout<<endl;
}

int List::size()
{
    return this->size_;
}

