class Deque
{
  float *items_;
  int first_;
  int last_;
  int capacity_;
  int size_;

  void move(int x);
  void memAlloc(int newcapacity_);

public:
  Deque();
  ~Deque();
  void pushFront(float value);
  void pushBack(float value);
  void popFront();
  void popBack();
  void print();

  int size();
  float getFromFront();
  float getFromEnd();
};
