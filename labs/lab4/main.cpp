#include <cstdlib>
#include <string>

#include "list.h"
#include "deque.h"

using namespace std;

void fromListToDeque(List & list, Deque & deq1, Deque & deq2)
{
    for (int i = 0; i < list.size(); i++)
    {
        if (i % 2 != 0)
        {
            deq1.pushFront(list[i]);
        }
        else
        {
            deq2.pushBack(list[i]);
        }
    }
}

void fromDequeToList(List & list, Deque & deq1, Deque & deq2)
{
    while (deq1.size() != 0)
    {
        list.add(deq1.getFromEnd());
        deq1.popBack();
    }
    while (deq2.size() != 0)
    {
        list.add(deq2.getFromFront());
        deq2.popFront();
    }
}

void fillFromString(List *list, FILE *fp)
{
    char p[1000];
    fgets(p, 1000, fp);
    char p1[1000];
    int i = 0;
    int k = 0;
    int l = 0;
    do
    {
        if (p[k] == ' ' || p[k + 1] == '\0')
        {
            p1[i] = '\0';
            list->add(atof(p1));
            i = 0;
            k++;
        }
        else
        {
            p1[i] = p[k];
            k++;
            i++;
        }
    } while (p[k] != '\0');
}

void negativeNumbersOnFirstPos(List *self)
{
    int counter = 0;
    float tmp = 0;
    for (int i = 0; i < self->size(); i++)
    {
        if ((*self)[i] < 0)
        {
            tmp = (*self)[i];
            (*self)[i] = (*self)[counter];
            (*self)[counter] = tmp;
            counter++;
        }
    }
}


int main()
{
    FILE *fp;
    fp = fopen("data.txt", "r");
    if (fp == NULL)
    {
        fprintf(stderr, "Error opening file input.txt\n");
        return 1;
    }
    List list;
    fillFromString(&list, fp);
    list.print();
    cout << endl;
    
    negativeNumbersOnFirstPos(&list);
    list.print();
    cout << endl;

    Deque deq1;
    
    Deque deq2;
    
    fromListToDeque(list, deq1, deq2);
    deq1.print();
    cout << endl;

    deq2.print();
    cout << endl;

    List list1;

    fromDequeToList(list1, deq1, deq2);
    list1.print();

    fclose(fp);
    
    return 0;
}
