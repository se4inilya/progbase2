#include <iostream>
#include <cstdio>

class List
{
    int cap_;
    int size_;
    float *arr_;

    void memAlloc(int newcap_);

  public:
    List();
    ~List();
    void add(float newValue);
    void print();
    int size();

    float & operator [](int index);
};

