#pragma once

#include <vector>
#include <string>

#include "optional.h"
#include "book.h"
#include "author.h"
#include "csv.h"

using std::string;
using std::vector;

class Storage
{
   const string dir_name_;

   vector<Book> books_;
   vector<Author> authors_;

   static Book rowToBook(const CsvRow &row);
   static CsvRow bookToRow(const Book &st);
   static Author rowToAuthor(const CsvRow &row);
   static CsvRow authorToRow(const Author &cs);

   int getNewBookId();
   int getNewAuthorId();

 public:
   Storage(const string & dir_name) : dir_name_(dir_name) { }

   bool load();
   bool save();
   // books
   vector<Book> getAllBooks(void);
   optional<Book> getBookById(int book_id);
   bool updateBook(const Book &book);
   bool removeBook(int book_id);
   int insertBook(const Book &book);
   // Authors
   vector<Author> getAllAuthors(void);
   optional<Author> getAuthorById(int author_id);
   bool updateAuthor(const Author &author);
   bool removeAuthor(int author_id);
   int insertAuthor(const Author &author);
};

