#include "storage.h"
#include "cui.h"
#include <iostream>

using namespace std;

int main()
{

    Storage storage("./data");
    storage.load();

    Cui cui(&storage);
    cui.show();

    return 0;
}

