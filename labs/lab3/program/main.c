#include <bstree.h>
#include <assert.h>

char *String_allocCopy(char *value)
{
    char *p = StrOnHeap(value);
    return p;
}
char *String_allocFromInt(int value)
{
    char buf[100];
    sprintf(buf, "%d", value);
    char *p = StrOnHeap(buf);
    return p;
}
char *String_allocFromDouble(double value)
{
    char buf[100];
    sprintf(buf, "%f", value);
    char *p = StrOnHeap(buf);
    return p;
}

char *keys[4] = {"id", "bookname", "authorname", "charactercapacity"};

StrStrMap *createBookMap(int id, char *bookname, char *authorname, int charactercapacity)
{
    // create values copies as strings on heap
    char *idValue = String_allocFromInt(id);
    char *booknameValue = String_allocCopy(bookname);
    char *authornameValue = String_allocCopy(authorname);
    char *charcapValue = String_allocFromInt(charactercapacity);
    // create map on heap and add values
    StrStrMap *map = StrStrMap_alloc();
    StrStrMap_add(map, "id", idValue);
    StrStrMap_add(map, "bookname", booknameValue);
    StrStrMap_add(map, "authorname", authornameValue);
    StrStrMap_add(map, "charactercapacity", charcapValue);
    // test checks
    assert(StrStrMap_contains(map, "id"));
    assert(StrStrMap_contains(map, "bookname"));
    assert(StrStrMap_contains(map, "authorname"));
    assert(StrStrMap_contains(map, "charactercapacity"));
    // return allocated instance
    return map;
}

List *CsvTable_toStrStrMapList(List *csvTable)
{
    List *StrStrMapList = List_alloc();

    for (int i = 0; i < List_size(csvTable); i++)
    {
        StrStrMap *map = StrStrMap_alloc();
        List *rowItem = List_get(csvTable, i);
        for (int j = 0; j < List_size(rowItem); j++)
        {
            char *value = List_get(rowItem, j);
            StrStrMap_add(map, keys[j], value);
        }
        List_add(StrStrMapList, map);
    }

    return StrStrMapList;
}

void List_MapList_deinit(List *self)
{

    for (int i = 0; i < List_size(self); i++)
    {

        StrStrMap_free(self->items[i]);
    }
    free(self->items);
    free(self);
}

int main(int argc, char *argv[])
{
    char data_name[20];
    data_name[0] = '\0';
    char out[20];
    out[0] = '\0';
    int n = 0;
    char str[1000];
    str[0] = '\0';
    bool iflag = false;
    bool oflag = false;
    bool bflag = false;

    int opt;
    while ((opt = getopt(argc, argv, "n:o:b")) != -1)
    {
        switch (opt)
        {
        case 'n':
            n = atoi(optarg);
            break;
        case 'o':
            printf("option: %c, value: %s\n", opt, optarg);
            strcpy(out, optarg);
            break;
        case 'b':
            bflag = true;
            break;
        case ':':
            printf("option needs a value\n");
            break;
        case '?':
            printf("unknown option: %c\n", optopt);
            break;
        }
    }

    if (optind < argc)
        strcpy(data_name, argv[optind]);

    for (; optind < argc; optind++)
    {
        printf("extra arguments: %s\n", argv[optind]);
    }

    FILE *p = NULL;
    if (strlen(data_name) > 0)
    {
        p = fopen(data_name, "r");
        iflag = true;
        if (p == NULL)
        {
            fprintf(stderr, "Cannot open \"%s\"\n", data_name);
            exit(EXIT_FAILURE);
        }
        char c = fgetc(p);
        int ind = 0;

        while (c != EOF)
        {
            str[ind++] = c;
            c = fgetc(p);
        }
        fclose(p);
        str[ind] = '\0';
    }

    if (strlen(out) > 0)
    {
        oflag = true;
    }

    if (iflag == true)
    {
        List inCsvTable;
        List_init(&inCsvTable);

        Csv_fillTableFromString(&inCsvTable, str);

        List *StrStrList = CsvTable_toStrStrMapList(&inCsvTable);

        if (oflag == 1)
        {
            FILE *pout = fopen(out, "w");
            for (int j = 0; j < List_size(StrStrList); j++)
            {
                StrStrMap *map = List_get(StrStrList, j);
                StrStrMap_displayToFile(map, pout);
            }
            fclose(pout);
        }
        else
        {
            printf("\nList of maps\n");
            for (int i = 0; i < List_size(StrStrList); i++)
            {
                StrStrMap *self = List_get(StrStrList, i);
                StrStrMap_display(self);
            }
        }

        if (bflag == true)
        {

            BSTree *bst1 = BSTree_init();

            for (int i = 0; i < List_size(StrStrList); i++)
            {
                BSTree_insert(bst1, List_get(StrStrList, i));
            }
            puts("");
            printf("Binary search tree:\n");
            BSTree_print(bst1);
            puts("");

            if (n > 0)
            {
                for (int i = 0; i < List_size(StrStrList); i++)
                {
                    const char *str = StrStrMap_get(List_get(StrStrList, i), keys[3]);
                    if (n <= atoi(str))
                    {
                        BSTree_deletenode(&bst1, atoi(StrStrMap_get(List_get(StrStrList, i), keys[0])));
                    }
                }
                printf("Editted binary search tree:\n");
                BSTree_print(bst1);
            }

            BSTree_deinit(bst1);
        }

        List_MapList_deinit(StrStrList);
        Csv_clearTable(&inCsvTable);
        List_deinit(&inCsvTable);
    }
    else
    {

        StrStrMap *map1 = createBookMap(16, "Three musketeers", "Alexander Duma", 52);
        StrStrMap *map2 = createBookMap(12, "Hound of the Baskervilles", "Arthur Conan Doyle", 10);
        StrStrMap *map3 = createBookMap(32, "Red and black", "Stendall", 7);
        StrStrMap *map4 = createBookMap(4, "Hobbit", "John Tolkien", 67);
        StrStrMap *map5 = createBookMap(54, "1984", "George Orwell", 20);
        List *table1 = List_alloc();
        List_add(table1, map1);
        List_add(table1, map2);
        List_add(table1, map3);
        List_add(table1, map4);
        List_add(table1, map5);
        int k = List_size(table1);

        if (oflag == 1)
        {
            FILE *pout1 = fopen(out, "w");
            for (int j = 0; j < List_size(table1); j++)
            {
                StrStrMap *map = List_get(table1, j);
                StrStrMap_displayToFile(map, pout1);
            }
            fclose(pout1);
        }
        else
        {
            printf("\nList of maps\n");
            for (int i = 0; i < List_size(table1); i++)
            {
                StrStrMap *self = List_get(table1, i);
                StrStrMap_display(self);
            }
        }

        if (bflag == true)
        {
            BSTree *bst1 = BSTree_init();

            for (int i = 0; i < List_size(table1); i++)
            {
                BSTree_insert(bst1, List_get(table1, i));
            }
            puts("");
            printf("Binary search tree:\n");
            BSTree_print(bst1);
            puts("");

            if (n > 0)
            {
                for (int i = 0; i < List_size(table1); i++)
                {
                    const char *str = StrStrMap_get(List_get(table1, i), keys[3]);
                    if (n <= atoi(str))
                    {
                        BSTree_deletenode(&bst1, atoi(StrStrMap_get(List_get(table1, i), keys[0])));
                    }
                }
                printf("Editted binary search tree:\n");
                BSTree_print(bst1);
            }

            BSTree_deinit(bst1);
        }

        for (int i = 0; i < List_size(table1); i++)
        {
            StrStrMap *iter = List_get(table1, i);
            StrStrMap_free(iter);
        }
        List_free(table1);
    }

    return 0;
}
