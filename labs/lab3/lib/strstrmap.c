#include "strstrmap.h"
#include <stdio.h>

StrStrMap *StrStrMap_alloc()
{
    StrStrMap *p = malloc(sizeof(StrStrMap));
    SortedKeyValueList_init(&p->list);
    return p;
}
void StrStrMap_free(StrStrMap *self)
{
    StrStrMap_deinit(self);
    free(self);
}

void StrStrMap_init(StrStrMap *self)
{
    SortedKeyValueList_init(&self->list);
}
void StrStrMap_deinit(StrStrMap *self)
{
    for (int i = 0; i < self->list.size; i++)
    {
        void *p = (void *)self->list.items[i].value;
        free(p);
    }
    SortedKeyValueList_deinit(&self->list);
}

size_t StrStrMap_size(StrStrMap *self)
{
    return SortedKeyValueList_size(&self->list);
}

void StrStrMap_add(StrStrMap *self, const char *key, char *value)
{
    SortedKeyValueList_add(&self->list, (KeyValue){key, value});
}
bool StrStrMap_contains(StrStrMap *self, const char *key)
{
    return SortedKeyValueList_contains(&self->list, (KeyValue){key});
}
const char *StrStrMap_get(StrStrMap *self, const char *key)
{
    int index = SortedKeyValueList_indexOf(&self->list, (KeyValue){key});
    if (index == -1)
    {
        fprintf(stderr, "Key `%s` not found\n", key);
        abort();
    }
    KeyValue kv = SortedKeyValueList_get(&self->list, index);
    return kv.value;
}
const char *StrStrMap_set(StrStrMap *self, const char *key, char *value)
{
    int index = SortedKeyValueList_indexOf(&self->list, (KeyValue){key});
    if (index == -1)
    {
        fprintf(stderr, "Key `%s` not found\n", key);
        abort();
    }
    KeyValue oldItem = SortedKeyValueList_get(&self->list, index);
    SortedKeyValueList_set(&self->list, index, (KeyValue){key, value});
    return oldItem.value;
}
const char *StrStrMap_remove(StrStrMap *self, const char *key)
{
    int index = SortedKeyValueList_indexOf(&self->list, (KeyValue){key});
    if (index == -1)
    {
        fprintf(stderr, "Key `%s` not found\n", key);
        abort();
    }
    KeyValue oldItem = SortedKeyValueList_get(&self->list, index);
    SortedKeyValueList_remove(&self->list, (KeyValue){key});
    return oldItem.value;
}
void StrStrMap_clear(StrStrMap *self)
{
    SortedKeyValueList_clear(&self->list);
}
void StrStrMap_display(StrStrMap *self)
{

    printf("[%s]", StrStrMap_get(self, "id"));
    printf("[%s]", StrStrMap_get(self, "bookname"));
    printf("[%s]", StrStrMap_get(self, "authorname"));
    printf("[%s]", StrStrMap_get(self, "charactercapacity"));
    printf("\n");
}

void StrStrMap_displayToFile(StrStrMap *map, FILE * fp)
{

    fprintf(fp, "%s,", StrStrMap_get(map, "id"));
    fprintf(fp, "%s,", StrStrMap_get(map, "bookname"));
    fprintf(fp, "%s,", StrStrMap_get(map, "authorname"));
    fprintf(fp, "%s", StrStrMap_get(map, "charactercapacity"));
 
    fprintf(fp, "\n");
}