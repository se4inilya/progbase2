#include "bintree.h"
#include <stdlib.h>

BinTree *BinTree_alloc(StrStrMap *map)
{
    BinTree *self = (BinTree *)malloc(sizeof(BinTree));
    if (self != NULL)
    {
        self->key = atoi(StrStrMap_get(map, "id"));
        self->map = map;
        self->left = NULL;
        self->right = NULL;
    }

    return self;
}

void BinTree_free(BinTree **root)
{
    if (*root == NULL)
    {
        return;
    }
    BinTree_free(&(*root)->left);
    BinTree_free(&(*root)->right);
    free(*root);
    
}

void BinTree_insert(BinTree **root, StrStrMap *map)
{
    int key = atoi(StrStrMap_get(map, "id"));
    if (*root == NULL)
    {

        *root = BinTree_alloc(map);
    }
    else
    {
        if (key < (*root)->key)
        {
            BinTree_insert(&(*root)->left, map);
        }
        else if (key > (*root)->key)
        {
            BinTree_insert(&(*root)->right, map);
        }
    }
}

void BinTree_inorder(BinTree *root) 
{ 
    if (root != NULL) 
    { 
       
        BinTree_inorder(root->left); 
         StrStrMap_display(root->map);
         puts("");
        
        BinTree_inorder(root->right); 
    } 
} 

BinTree* BinTree_search(BinTree *root, int key) 
{ 
    // Base Cases: root is null or key is present at root 
    if (root == NULL || root->key == key) 
       return root; 
     
    // Key is greater than root's key 
    if (root->key < key) 
       return BinTree_search(root->right, key); 
  
    // Key is smaller than root's key 
    return BinTree_search(root->left, key); 
} 

BinTree *BinTree_deleteNode(BinTree *root, int key)
{
     // base case
     if (root == NULL) return root;

    // If the key to be deleted is smaller than the root's key,
    // then it lies in left subtree
    if (key < root->key)
        root->left = BinTree_deleteNode(root->left, key);

    // If the key to be deleted is greater than the root's key,
    // then it lies in right subtree
    else if (key > root->key)
        root->right = BinTree_deleteNode(root->right, key);

    // if key is same as root's key, then This is the node
    // to be deleted
    else
    {
        // node with only one child or no child
        if (root->left == NULL)
        {
            BinTree *temp = root->right;
            free(root);
            return temp;
        }
        else if (root->right == NULL)
        {
            BinTree *temp = root->left;
            free(root);
            return temp;
        } 

        // node with two children: Get the BinTree_inorder successor (smallest
        // in the right subtree)
        BinTree *temp = BinTree_minValueNode(root->right);

        // Copy the BinTree_inorder successor's content to this node
        root->key = temp->key;
        root->map = temp->map;

        // Delete the BinTree_inorder successor
        root->right = BinTree_deleteNode(root->right, temp->key);
    }
    return root;
}

BinTree *BinTree_minValueNode(BinTree *node) 
{
BinTree *current = node; 
  
/* loop down to find the leftmost leaf */
while (current->left != NULL)  
{ 
    current = current->left; 
} 
return(current); 
} 
