#include "csv.h"

void Csv_addInt(List *row, int value)
{
    if (row->size == row->capacity)
    {
        row->capacity *= 2;
        void **new_val = realloc(row->items, sizeof(char *) * row->capacity);
        if (new_val == NULL)
        {
            printf("ERROR. CAN NOT REALLOCATE\n");
            List_free(row);
            exit(1);
        }
        row->items = new_val;
    }

    const int len = 1;
    char a[len];
    sprintf(a, "%d", value);
    row->items[row->size++] = StrOnHeap(a);
}

void Csv_addDouble(List *row, double value)
{
    if (row->size == row->capacity)
    {
        row->capacity *= 2;
        void **new_val = realloc(row->items, sizeof(char *) * row->capacity);
        if (new_val == NULL)
        {
            printf("ERROR. CAN NOT REALLOCATE\n");
            List_free(row);
            exit(1);
        }
        row->items = new_val;
    }

    const int len = 1;
    char a[len];
    sprintf(a, "%.2f", value);
    row->items[row->size++] = StrOnHeap(a);
}

void Csv_addString(List *row, const char *value)
{
    if (row->size == row->capacity)
    {
        row->capacity *= 2;
        void **new_val = realloc(row->items, sizeof(char *) * row->capacity);
        if (new_val == NULL)
        {
            printf("ERROR. CAN NOT REALLOCATE\n");
            List_free(row);
            exit(1);
        }
        row->items = new_val;
    }
    char *p = malloc(sizeof(char *) * strlen(value));
    strcpy(p, value);
    row->items[row->size++] = p;
}

int Csv_int(List *row, int index)
{
    int *k = List_get(row, index);
    return *k;
}

double Csv_double(List *row, int index)
{
    double *k = List_get(row, index);
    return *k;
}

int Csv_string(List *row, int index, char *b, int n)
{
    char *s = List_get(row, index);
    strcpy(b, s);
    return 0;
}

void Csv_addRow(List *table, List *row)
{
    if (table->size == table->capacity)
    {
        table->capacity *= 2;
        void **new_val = realloc(table->items, sizeof(char *) * table->capacity);
        if (new_val == NULL)
        {
            printf("ERROR. CAN NOT REALLOCATE\n");
            List_free(table);
            exit(1);
        }
        table->items = new_val;
    }
    table->items[table->size++] = row;
}

List *Csv_row(List *table, int index)
{
    return table->items[index];
}

// void Csv_fillTableFromString(List *csvTable, const char *csvString)
// {
    
//     List *row = NULL;
//     char buf[100];
//     int bufX = 0;
//     const char *p = csvString;
//     char tmp_str[100];
//     int count = 0;
//     while (1)
//     {
//         if (*p == '\r')
//             p++;
//         if (*p == ',' || *p == '\n' || *p == '\0')
//         {
//             buf[bufX] = '\0';

//             if (bufX == 0 && *p == '\0' && row == NULL)
//             {
//                 break;
//             }
//             if (row == NULL)
//             {
//                 row = List_alloc();
//             }
//             List_add(row, StrOnHeap(buf));
//             //printf("[%s]", buf);  // end-of-value
//             if (*p == '\n' || *p == '\0')
//             {
//                 List_add(csvTable, row);
//                 row = NULL;
//                 //printf("\n");  // end-of-row
//             }
//             //
//             bufX = 0;
//         }
        
//         else
//         {
//             buf[bufX] = *p;
//             bufX += 1;
//         }

//         //
//         if (*p == '\0')
//         {
//             break;
//         }
//         p += 1;
//     }
// }
void Csv_fillTableFromString(List *csvTable, const char *csvString)
{
    char buf[100];
    int bufX = 0;

    List *list1 = NULL;

    while (1)
    {
        if (*csvString == ',')
        {
            if (list1 == NULL)
            {
                list1 = List_alloc();
            }
            buf[bufX] = '\0';
            bufX = 0;
            Csv_addRow(list1, StrOnHeap(&buf[0]));
        }
        else if (*csvString == '\n')
        {
            if (bufX != 0)
            {
                buf[--bufX] = '\0';
                bufX = 0;
                Csv_addRow(list1, StrOnHeap(&buf[0]));
                Csv_addRow(csvTable, list1);
            }

            list1 = NULL;
        }
        else if (*csvString == '\0')
        {
            if (bufX == 0)
            {
                break;
            }
            if (list1 == NULL)
            {
                list1 = List_alloc();
            }

            buf[bufX] = '\0';
            Csv_addRow(list1, StrOnHeap(&buf[0]));
            Csv_addRow(csvTable, list1);
            break;
        }
        else if (*csvString == '\"')
        {
            csvString += 1;
            if (*(csvString) == '\"')
            {
                buf[bufX++] = *csvString;
            }
            else
            {
                while (*csvString != '"')
                {
                    if (*csvString == '\n')
                    {
                        buf[bufX++] = '\\';
                        buf[bufX++] = 'n';
                        csvString += 1;
                    }
                    else
                    {
                        buf[bufX++] = *csvString;
                        csvString += 1;
                    }
                }
            }
        }
        else
        {
            buf[bufX++] = *csvString;
        }

        csvString += 1;
    }
}

int Csv_fillStringFromTable(List *csvTable, char *b, int n)
{
    b[0] = '\0';
    for (int i = 0; i < List_size(csvTable); i++)
    {
        List *rowItem = List_get(csvTable, i);
        for (int j = 0; j < List_size(rowItem); j++)
        {
            char *value = List_get(rowItem, j);
            strcat(b, value);
            if (j != List_size(rowItem) - 1)
            {
                strcat(b, ",");
            }
        }
        if (i != List_size(csvTable) - 1)
        {
            strcat(b, "\n");
        }
    }
    return 0;
}

char *Csv_createStringFromTable(List *csvTable)
{
    char *str = malloc(sizeof(char *) * Csv_sizeOfString(csvTable) - 1); //?????
    str[0] = '\0';

    for (int i = 0; i < List_size(csvTable); i++)
    {
        List *rowItem = List_get(csvTable, i);

        for (int j = 0; j < List_size(rowItem); j++)
        {
            char *item = List_get(rowItem, j);

            strcat(str, item);
            if (j != List_size(rowItem) - 1)
            {
                strcat(str, ",");
            }
        }
        if (i != List_size(rowItem))
        {
            strcat(str, "\n");
        }
    }
    return str;
}

void Csv_clearTable(List *csvTable)
{
    for (int i = 0; i < List_size(csvTable); i++)
    {
        void *l1 = List_get(csvTable, i);

        for (int j = 0; j < List_size(l1); j++)
        {
            char *str = List_get(l1, j);
            //free(str);
        }
        List_free(l1);
    }
    //List_free(csvTable);
}

void Csv_printTable(List *table)
{
    for (int i = 0; i < List_size(table); i++)
    {
        List *l = Csv_row(table, i);
        if (List_size(l) == 0)
        {
        }
        else
        {
            for (int j = 0; j < List_size(l); j++)
            {
                char *s = List_get(l, j);
                printf("[%s]", s);
            }
            printf("\n");
        }
    }
}

void Csv_printTableToFile(List *table)
{
    for (int i = 0; i < List_size(table); i++)
    {
        List *l = Csv_row(table, i);
        if (List_size(l) == 0)
        {
        }
        else
        {
            for (int j = 0; j < List_size(l); j++)
            {
                char *s = List_get(l, j);
                if (j == List_size(l) - 1)
                {
                    printf("%s", s);
                }
                else
                {
                    printf("%s,", s);
                }
            }
            printf("\n");
        }
    }
}

int Csv_sizeOfString(List *self)
{
    int size = 0;

    for (int i = 0; i < List_size(self); i++)
    {
        List *rowItem = List_get(self, i);

        for (int j = 0; j < List_size(rowItem); j++)
        {
            char *item = List_get(rowItem, j);

            while (*item != '\0')
            {
                size += 1;
                item += 1;
            }
        }
    }

    return size;
}

