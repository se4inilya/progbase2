#include "csv.h"
#include "strstrmap.h"
#pragma once

typedef struct BinTree BinTree;
typedef int (*comparer)(int, int);

int compare(int left,int right);

struct BinTree
{
    int key;
    StrStrMap *map;
    BinTree *left;
    BinTree *right;
};

BinTree *BinTree_alloc(StrStrMap *map);
void BinTree_free(BinTree **root);
void BinTree_insert(BinTree **root, StrStrMap *map);
void BinTree_inorder(BinTree *root);
BinTree* BinTree_search(BinTree *root, int key);
BinTree *BinTree_deleteNode(BinTree *root, int key);
BinTree *BinTree_minValueNode(BinTree *node);

