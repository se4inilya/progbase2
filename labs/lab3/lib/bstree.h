#include "bintree.h"
#include <stdlib.h>
#include <stdbool.h>
#pragma once

typedef struct BSTree BSTree;

struct BSTree
{
    BinTree *root;
    size_t size;
};

BSTree *BSTree_init();
void BSTree_deinit(BSTree *self);
void BSTree_insert(BSTree *self, StrStrMap *map);
void BSTree_deletenode(BSTree **root, int key);
void BSTree_print(BSTree * self);