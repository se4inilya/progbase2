#include "list.h"

struct Book
{
    int id;
    char *book_name;
    char *author_name;
    int character_capacity;
};

typedef struct Book Book;

void Csv_addInt(List *row, int value);            //
void Csv_addDouble(List *row, double value);      //
void Csv_addString(List *row, const char *value); //
void Csv_addRow(List *table, List *row);          //

int Csv_int(List *row, int index);                    //
double Csv_double(List *row, int index);              //
int Csv_string(List *row, int index, char *b, int n); //

List *Csv_row(List *table, int index); //

void Csv_fillTableFromString(List *csvTable, const char *csvString); //
int Csv_fillStringFromTable(List *csvTable, char *b, int n);         //
char *Csv_createStringFromTable(List *csvTable);
void Csv_clearTable(List *csvTable);                                 //

void Csv_printTable(List *self); //
void Csv_printTableToFile(List *table);
void print_books_array(List *table, bool outind);

int Csv_sizeOfString(List *self); //