#include <stdio.h>
#include "bstree.h"

BSTree *BSTree_init()
{
    BSTree *self = malloc(sizeof(BSTree));
    self->root = NULL;
    self->size = 0;
    return self;
}

void BSTree_deinit(BSTree *self)
{
    BinTree_free(&self->root);
    free(self);
}

void BSTree_insert(BSTree *self, StrStrMap *map)
{
    self->size++;
    BinTree_insert(&self->root, map);
}

void BSTree_deletenode(BSTree **root, int key)
{
    (*root)->root = BinTree_deleteNode((*root)->root, key);
}

static void printValueOnDepth(BinTree * node, char pos, int depth)
{
   for (int i = 0; i < depth; i++) {
       printf("....");
   }
   printf("%c: ", pos); 
// 
   if (node == NULL) {
       printf("(null)\n");
   } else {
       printf("[%i] `%s`\n", node->key, StrStrMap_get(node->map, "bookname") );
   }
}
// 
static void printNode(BinTree * node, char pos, int depth)
{
    bool hasChild = node != NULL && (node->left != NULL || node->right != NULL);
    if (hasChild) printNode(node->right, 'R', depth + 1);
    printValueOnDepth(node, pos, depth);
    if (hasChild) printNode(node->left, 'L', depth + 1);
}
// 
void BSTree_print(BSTree * self)
{
    printNode(self->root, '+', 0);
}