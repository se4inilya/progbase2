#include "csv_storage.h"

class Cui
{
    Storage * const storage_;

    void mainMenu();
    // books menus
    void booksMainMenu();
    void bookMenu(int entity_id);
    void bookUpdateMenu(int entity_id);
    void bookDeleteMenu(int entity_id);
    void bookCreateMenu();
    //authors
    void authorsMainMenu();
    void authorMenu(int entity_id);
    void authorUpdateMenu(int entity_id);
    void authorDeleteMenu(int entity_id);
    void authorCreateMenu();
    
public:
    Cui(Storage * storage): storage_(storage) {}
    //
    void show();
};