#include "cui.h"
#include <iostream>
#include <iomanip>

using namespace std;

void Cui::mainMenu()
{
    cout << "1. Books menu" << endl
         << "2. Authors menu" << endl
         << "0. Exit" << endl;
}
void Cui::booksMainMenu()
{
    vector<Book> books = this->storage_->getAllBooks();
    cout << endl;
    for (Book &b : books)
    {
        cout << setw(2) << b.id << " | " << setw(40) << b.book_name << " | " << setw(25) << b.author_name << " | " << setw(2) << b.character_capacity << endl;
    }
    cout << endl
         << "1. Choose an entity to interact" << endl
         << "2. Insert the entity" << endl
         << "0. Exit" << endl;
}
void Cui::bookMenu(int entity_id)
{
    if (storage_->getBookById(entity_id) == nullopt)
    {
        cout << "Element with this id does not exist." << endl;
        return;
    }
    cout << "Functions with entity :" << endl
         << "1. Update" << endl
         << "2.Delete" << endl
         << "0. Exit" << endl;
    int opt3 = 0;
    (cin >> opt3).get();
    if (opt3 == 1)
    {
        this->bookUpdateMenu(entity_id);
    }
    else if (opt3 == 2)
    {
        this->bookDeleteMenu(entity_id);
    }
    else if (opt3 == 0)
        return;
    else
    {
        cout << "Unknown option" << endl;
    }
}
void Cui::bookUpdateMenu(int entity_id)
{
    if (storage_->getBookById(entity_id) == nullopt)
    {
        cout << "Element with this id does not exist." << endl;
        return;
    }
    Book b;
    b.id = entity_id;
    string bookname;
    cout << "Enter book name :" << endl;
    getline(cin, bookname);
    b.book_name = bookname;
    string authorname;
    cout << "Enter author name :" << endl;
    getline(cin, authorname);
    b.author_name = authorname;
    cout << "Enter character capacity :" << endl;
    int cc;
    (cin >> cc).get();
    b.character_capacity = cc;
    this->storage_->updateBook(b);
}
void Cui::bookDeleteMenu(int entity_id)
{
    if (storage_->getBookById(entity_id) == nullopt)
    {
        cout << "Element with this id does not exist." << endl;
        return;
    }
    this->storage_->removeBook(entity_id);
}
void Cui::bookCreateMenu()
{
    Book b;
    b.id = 0;
    cout << "Enter book name :" << endl;
    string bookname;
    getline(cin, bookname);
    b.book_name = bookname;
    string authorname;
    cout << "Enter author name :" << endl;
    getline(cin, authorname);
    b.author_name = authorname;
    cout << "Enter character capacity :" << endl;
    int cc;
    (cin >> cc).get();
    b.character_capacity = cc;
    this->storage_->insertBook(b);
}
void Cui::authorsMainMenu()
{
    vector<Author> authors = this->storage_->getAllAuthors();
    cout << endl;
    for (Author &b : authors)
    {
        cout << setw(2) << b.id << " | " << setw(25) << b.authorName << " | " << setw(4) << b.yearOfBirth << " | " << ((b.yearOfDeath == -1) ? "author is alive" : to_string(b.yearOfDeath)) << endl;
    }
    cout << endl
         << "1. Choose an entity to interact" << endl
         << "2. Insert the entity" << endl
         << "0. Exit" << endl;
}
void Cui::authorMenu(int entity_id)
{
    if (storage_->getAuthorById(entity_id) == nullopt)
    {
        cout << "Element with this id does not exist." << endl;
        return;
    }
    cout << "Functions with entity :" << endl
         << "1. Update" << endl
         << "2.Delete" << endl
         << "0. Exit" << endl;
    int opt3 = 0;
    (cin >> opt3).get();
    if (opt3 == 1)
    {
        this->authorUpdateMenu(entity_id);
    }
    else if (opt3 == 2)
    {
        this->authorDeleteMenu(entity_id);
    }
    else if (opt3 == 0)
        return;
    else
    {
        cout << "Unknown option" << endl;
    }
}
void Cui::authorUpdateMenu(int entity_id)
{
    if (storage_->getAuthorById(entity_id) == nullopt)
    {
        cout << "Element with this id does not exist." << endl;
        return;
    }
    Author b;
    b.id = entity_id;
    string authorname;
    cout << "Enter author name :" << endl;
    getline(cin, authorname);
    b.authorName = authorname;
    cout << "Enter year of birth :" << endl;
    int yob;
    (cin >> yob).get();
    b.yearOfBirth = yob;

    cout << "Enter year of death :" << endl;
    int death_year;
    (cin >> death_year).get();
    b.yearOfDeath = death_year;

    this->storage_->updateAuthor(b);
}
void Cui::authorDeleteMenu(int entity_id)
{
    if (storage_->getAuthorById(entity_id) == nullopt)
    {
        cout << "Element with this id does not exist." << endl;
        return;
    }
    this->storage_->removeAuthor(entity_id);
}
void Cui::authorCreateMenu()
{
    Author b;
    b.id = 0;
    string authorname;
    cout << "Enter author name :" << endl;
    getline(cin, authorname);
    b.authorName = authorname;
    cout << "Enter year of birth :" << endl;
    int yob;
    (cin >> yob).get();
    b.yearOfBirth = yob;

    cout << "Enter year of death :" << endl;
    int death_year;
    (cin >> death_year).get();
    b.yearOfDeath = death_year;
    this->storage_->insertAuthor(b);
}
void Cui::show()
{
    while (1)
    {
        this->mainMenu();
        int opt = 0;
        (cin >> opt).get();
        if (opt == 1)
        {
            while (1)
            {
                this->booksMainMenu();
                int opt1 = 0;
                (cin >> opt1).get();
                if (opt1 == 1)
                {
                    cout << "Enter an id of entity" << endl;
                    int opt2 = 0;
                    (cin >> opt2).get();
                    this->bookMenu(opt2);
                }
                else if (opt1 == 2)
                {
                    this->bookCreateMenu();
                }
                else if (opt1 == 0)
                    break;
                else
                {
                    cout << "Unknown option" << endl;
                }
            }
        }
        else if (opt == 2)
        {
            while (1)
            {
                this->authorsMainMenu();
                int opt1 = 0;
                (cin >> opt1).get();
                //cout << "your optiuon: ";

                if (opt1 == 1)
                {
                    cout << "Enter an id of entity" << endl;
                    int opt2 = 0;
                    (cin >> opt2).get();

                    this->authorMenu(opt2);
                }
                else if (opt1 == 2)
                {
                    this->authorCreateMenu();
                }
                else if (opt1 == 0)
                    break;
                else
                {
                    cout << "Unknown option" << endl;
                }
            }
        }
        else if (opt == 0)
        {
            break;
        }
        else
        {
            cout << "Unknown option" << endl;
        }
    }
    this->storage_->save();
}