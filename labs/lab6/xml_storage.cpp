#include "xml_storage.h"

#include <iostream>
#include <fstream>

using namespace std;

Author QDomElementToAuthor(QDomElement & element)
{
    Author author;
    author.id = element.attribute("id").toInt();;
    author.authorName = element.attribute("authorName").toStdString();
    author.yearOfBirth = element.attribute("yearOfBirth").toInt();
    author.yearOfDeath = element.attribute("yearOfDeath").toInt();
    return author;
}

QDomElement AuthorToDomElement(QDomDocument & doc, Author & author)
{
    QDomElement author_el = doc.createElement("author");
    author_el.setAttribute("id", author.id);
    author_el.setAttribute("authorName", author.authorName.c_str());
    author_el.setAttribute("yearOfDeath", author.yearOfDeath);
    author_el.setAttribute("yearOfBirth", author.yearOfBirth);
    return author_el;
}


Book QDomElementToBook(QDomElement & element)
{
    Book book;
    book.id = element.attribute("id").toInt();
    book.book_name = element.attribute("book_name").toStdString();
    book.author_name = element.attribute("author_name").toStdString();
    book.character_capacity = element.attribute("character_capacity").toInt();
    return book;
}

QDomElement BookToDomElement(QDomDocument & doc, Book & book)
{
    QDomElement book_el = doc.createElement("book");
    book_el.setAttribute("id", book.id);
    book_el.setAttribute("book_name", book.book_name.c_str());
    book_el.setAttribute("author_name", book.author_name.c_str());
    book_el.setAttribute("character_capacity", book.character_capacity);
    return book_el;
}

bool XmlStorage::load()
{
    //Author
    string file_name = this->dir_name_ + "authors.xml";
    QString Qfile_name = QString::fromStdString(file_name);
    QFile file(Qfile_name);
    if (!file.open(QFile::ReadOnly))
    {
        qDebug() << "Error. Can not open " << Qfile_name << endl;
        return false;
    }
    QTextStream ts(&file);
    QString text = ts.readAll();
    file.close();

    QDomDocument doc;
    QString errorMsg;
    int errorLine;
    int errorCol;
    if(!doc.setContent(text, &errorMsg, &errorLine, &errorCol))
    {
        qDebug() << "Error parsing Xml text:" << errorMsg;
        qDebug() << "line: " << errorLine << "col: " << errorCol;
        return false;
    }
    QDomElement root = doc.documentElement();
    for (int i = 0; i < root.childNodes().size(); i++)
    {
        QDomNode node = root.childNodes().at(i);
        QDomElement element = node.toElement();
        Author author = QDomElementToAuthor(element);
        this->authors_.push_back(author);
    }

    //Book
    file_name = this->dir_name_ + "books.xml";
    Qfile_name = QString::fromStdString(file_name);
    QFile file1(Qfile_name);
    if (!file1.open(QFile::ReadOnly))
    {
        qDebug() << "Error. Can not open " << Qfile_name << endl;
        return false;
    }
    QTextStream ts1(&file1);
    QString text1 = ts1.readAll();
    file1.close();

    if(!doc.setContent(text1, &errorMsg, &errorLine, &errorCol))
    {
        qDebug() << "Error parsing Xml text:" << errorMsg;
        qDebug() << "line: " << errorLine << "col: " << errorCol;
        return false;
    }
    root = doc.documentElement();
    for (int i = 0; i < root.childNodes().size(); i++)
    {
        QDomNode node = root.childNodes().at(i);
        QDomElement element = node.toElement();
        Book book = QDomElementToBook(element);
        this->books_.push_back(book);
    }

    return true;
}

bool XmlStorage::save()
{
    //Author
    QDomDocument doc;
    QDomElement root = doc.createElement("authors");
    for (Author & a: this->authors_)
    {
        QDomElement author_el = AuthorToDomElement(doc, a);
        root.appendChild(author_el);
    }
    doc.appendChild(root);
    QString xml_text = doc.toString(4);

    string file_name = this->dir_name_ + "authors.xml";
    QString Qfile_name = QString::fromStdString(file_name);
    QFile file(Qfile_name);
    if (!file.open(QFile::WriteOnly))
    {
        qDebug() << "Error. Can not open " << Qfile_name << " for writing" << endl;
        return false;
    }

    QTextStream ts(&file);
    ts << xml_text;
    file.close();

    //Painting
    QDomDocument doc1;
    root = doc1.createElement("books");
    for (Book & b: this->books_)
    {
        QDomElement book_el = BookToDomElement(doc1, b);
        root.appendChild(book_el);
    }
    doc1.appendChild(root);
    QString xml_text1 = doc1.toString(4);

    file_name = this->dir_name_ + "books.xml";
    Qfile_name = QString::fromStdString(file_name);
    QFile file1(Qfile_name);
    if (!file1.open(QFile::WriteOnly))
    {
        qDebug() << "Error. Can not open " << Qfile_name << " for writing" << endl;
        return false;
    }

    QTextStream ts1(&file1);
    ts1 << xml_text1;
    file1.close();

    return true;
}

vector<Book> XmlStorage::getAllBooks(void)
{
    return this->books_;
}
optional<Book> XmlStorage::getBookById(int book_id)
{
    for (Book &b : this->books_)
    {
        if (b.id == book_id)
        {
            return b;
        }
    }
    return nullopt;
}
bool XmlStorage::updateBook(const Book &book)
{
    int size = this->books_.size();
    for (int i = 0; i < size; i++)
    {
        if (this->books_[i].id == book.id)
        {
            this->books_[i].book_name = book.book_name;
            this->books_[i].author_name = book.author_name;
            this->books_[i].character_capacity = book.character_capacity;
            return true;
        }
    }
    return false;
}
bool XmlStorage::removeBook(int book_id)
{
    int index = -1;
    int size = this->books_.size();
    for (int i = 0; i < size; i++)
    {
        if (this->books_[i].id == book_id)
        {
            index = i;
            break;
        }
    }
    if (index >= 0)
    {
        this->books_.erase(this->books_.begin() + index);
        return true;
    }
    return false;
}
int XmlStorage::insertBook(const Book &book)
{
    Book copy = book;
    copy.id = this->getNewBookId();
    this->books_.push_back(copy);
    return copy.id;
}

int XmlStorage::getNewBookId()
{
    int max = 0;
    for (Book &b : this->books_)
    {
        if (max < b.id)
        {
            max = b.id;
        }
    }
    max++;
    return max;
}

vector<Author> XmlStorage::getAllAuthors(void)
{
    return this->authors_;
}
optional<Author> XmlStorage::getAuthorById(int author_id)
{
    for (Author &a : this->authors_)
    {
        if (a.id == author_id)
        {
            return a;
        }
    }
    return nullopt;
}
bool XmlStorage::updateAuthor(const Author &author)
{
    int size = this->authors_.size();
    for (int i = 0; i < size; i++)
    {
        if (this->authors_[i].id == author.id)
        {
            this->authors_[i].authorName = author.authorName;
            this->authors_[i].yearOfBirth = author.yearOfBirth;
            this->authors_[i].yearOfDeath = author.yearOfDeath;
            return true;
        }
    }
    return false;
}
bool XmlStorage::removeAuthor(int author_id)
{
    int index = -1;
    int size = this->authors_.size();
    for (int i = 0; i < size; i++)
    {
        if (this->authors_[i].id == author_id)
        {
            index = i;
            break;
        }
    }
    if (index >= 0)
    {
        this->authors_.erase(this->authors_.begin() + index);
        return true;
    }
    return false;
}
int XmlStorage::insertAuthor(const Author &author)
{
    Author copy = author;
    copy.id = this->getNewAuthorId();
    this->authors_.push_back(copy);
    return copy.id;
}
int XmlStorage::getNewAuthorId()
{
    int max = 0;
    for (Author &a : this->authors_)
    {
        if (max < a.id)
        {
            max = a.id;
        }
    }
    max++;
    return max;
}

