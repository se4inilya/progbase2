#pragma once
#include <string>

struct Author
{
    int id;
    std::string authorName;
    int yearOfBirth;
    int yearOfDeath;
};
