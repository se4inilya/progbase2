#include "csv_storage.h"
#include "xml_storage.h"
#include "cui.h"
#include <iostream>

using namespace std;

int main()
{

    XmlStorage xml_storage("../lab6/data/xml/");
    Storage * storage_ptr = &xml_storage;
    storage_ptr->load();

    Cui cui(storage_ptr);
    cui.show();


    return 0;
}

