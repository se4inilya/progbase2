#ifndef AUTHDIALOG_H
#define AUTHDIALOG_H

#include <QDialog>
#include <QMainWindow>
#include <QListWidget>
#include <QDebug>
#include <QFileDialog>
#include <QString>
#include <QtGui>
#include <string.h>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include "user.h"
#include "mainwindow.h"

namespace Ui {
class AuthDialog;
}

class AuthDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AuthDialog(QWidget *parent = 0);
    ~AuthDialog();
signals:
    void sendUserData(User);
private slots:
    void on_OK_button_clicked();

    void on_Cancel_button_clicked();

private:
    Ui::AuthDialog *ui;
    MainWindow * mainwindow;
};

#endif // AUTHDIALOG_H
