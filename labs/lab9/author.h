#pragma once
#include <string>
#include <qmetaobject.h>

struct Author
{
    int id;
    std::string authorName;
    int yearOfBirth;
    int yearOfDeath;
};

Q_DECLARE_METATYPE(Author)
