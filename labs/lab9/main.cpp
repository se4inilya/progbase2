#include "mainwindow.h"
#include "authdialog.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    AuthDialog auth;
    auth.show();

    return a.exec();
}
