#include "updatedialog.h"
#include "ui_updatedialog.h"

#include <string>

#include "mainwindow.h"

UpdateDialog::UpdateDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UpdateDialog)
{
    ui->setupUi(this);
    ui->delete_button->setEnabled(false);
}

UpdateDialog::~UpdateDialog()
{
    delete ui;
}

void UpdateDialog::on_pushButton_clicked()  //OK
{
    Book* book = new Book();

    book->book_name = ui->lineEdit->text().toStdString();
    book->author_name = ui->lineEdit_2->text().toStdString();
    book->character_capacity = ui->spinBox->value();


    emit(sendUpdatedSignal(book));

    UpdateDialog::close();
}


void UpdateDialog::on_pushButton_2_clicked()  //Cancel
{
    UpdateDialog::close();
}

void UpdateDialog::receiveBookToEdit(QListWidgetItem * item, Storage * storage){
    QVariant qvar = item->data(Qt::UserRole);
    Book book = qvar.value<Book>();

    storage_ = storage;
    book_ = book;

    ui->lineEdit->setText(QString::fromStdString(book.book_name));
    ui->lineEdit_2->setText(QString::fromStdString(book.author_name));
    ui->spinBox->setValue(book.character_capacity);

    vector<Author> authors = storage_->getAllBookAuthors(book.id);
    for(int i = 0; i < authors.size(); i++){
        QListWidget *listWidget = ui->listWidget;
        QListWidgetItem *authorItem = new QListWidgetItem;

        QVariant qvar;
        qvar.setValue(authors.at(i));
        QString name = QString::fromStdString(authors.at(i).authorName);
        authorItem->setText(name);
        authorItem->setData(Qt::UserRole, qvar);

        listWidget->addItem(authorItem);
    }

}

void UpdateDialog::on_insert_button_clicked()
{
    insert_window = new InsertDialog(this);
    insert_window->show();

    connect(this, SIGNAL(sendStorage(Storage*, Book *)), insert_window, SLOT(receiveStorage(Storage*, Book *)));
    connect(insert_window, SIGNAL(sendupdatedAuthorList(Storage*)), this, SLOT(receiveupdatedAuthorList(Storage*)));

    emit(sendStorage(storage_, &book_));
}

void UpdateDialog::on_delete_button_clicked()
{
    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();

    foreach (QListWidgetItem * item, items)
    {
        items = ui->listWidget->selectedItems();
        QVariant var = item->data(Qt::UserRole);
        Author a = var.value<Author>();
        int ind = a.id;
        qDebug() << "ind: " << ind;
        delete ui->listWidget->takeItem(ui->listWidget->row(item));

        if(!storage_->removeBookAuthor(book_.id, a.id))
            qDebug() << "Can't remove author";
    }
}

void UpdateDialog::on_listWidget_itemClicked(QListWidgetItem *item)
{
    ui->delete_button->setEnabled(true);

    QVariant variant = item->data(Qt::UserRole);
    Author a = variant.value<Author>();


    string yearofb = std::to_string(a.yearOfBirth);
    string yearofd = std::to_string(a.yearOfDeath);
    ui->author_label->setText(QString::fromStdString(a.authorName));
    ui->yearofbirth_label->setText(QString::fromStdString(yearofb));
    ui->yearofdeath_label->setText(QString::fromStdString(yearofd));
}

void UpdateDialog::receiveupdatedAuthorList(Storage * storage){
    ui->listWidget->clear();
    storage_ = storage;
    vector<Author> authors = storage_->getAllBookAuthors(book_.id);
    for(int i = 0; i < authors.size(); i++){
        QListWidget *listWidget = ui->listWidget;
        QListWidgetItem *authorItem = new QListWidgetItem;

        QVariant qvar;
        qvar.setValue(authors.at(i));
        QString name = QString::fromStdString(authors.at(i).authorName);
        authorItem->setText(name);
        authorItem->setData(Qt::UserRole, qvar);

        listWidget->addItem(authorItem);
    }
}
