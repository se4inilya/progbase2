#ifndef UPDATEDIALOG_H
#define UPDATEDIALOG_H

#include <QDialog>
#include <QListWidget>
#include "book.h"
#include "storage.h"
#include "insertdialog.h"

namespace Ui {
class UpdateDialog;
}

class UpdateDialog : public QDialog
{
    Q_OBJECT

public:
    explicit UpdateDialog(QWidget *parent = 0);
    ~UpdateDialog();

signals:
    void sendUpdatedSignal(Book *);

    void sendStorage(Storage *, Book *);

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void receiveBookToEdit(QListWidgetItem *, Storage *);

    void on_insert_button_clicked();

    void on_delete_button_clicked();

    void on_listWidget_itemClicked(QListWidgetItem *item);

    void receiveupdatedAuthorList(Storage *);

private:
    Ui::UpdateDialog *ui;
    Storage * storage_;
    Book book_;
    InsertDialog * insert_window;
};

#endif // UPDATEDIALOG_H
