#ifndef ADDDIALOG_H
#define ADDDIALOG_H

#include <QDialog>
#include "book.h"

namespace Ui {
class AddDialog;
}

class AddDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddDialog(QWidget *parent = 0);
    ~AddDialog();
signals:
    void sendSignal(Book *);
private slots:
    void on_ok_button_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::AddDialog *ui;
};

#endif // ADDDIALOG_H
