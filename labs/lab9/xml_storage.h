#pragma once

#include <vector>
#include <string>
#include <QtXml>

#include "storage.h"
#include "optional.h"
#include "book.h"
#include "author.h"
#include "csv.h"
#include "QString"
#include "QFile"
#include "QDebug"

using std::string;
using std::vector;

class XmlStorage: public Storage
{
   const string dir_name_;

   vector<Book> books_;
   vector<Author> authors_;

   int getNewBookId();
   int getNewAuthorId();

 public:
   XmlStorage(const string & dir_name) : dir_name_(dir_name) { }

   bool open();
   bool close();
   // books
   vector<Book> getAllBooks(void);
   optional<Book> getBookById(int book_id);
   bool updateBook(const Book &book);
   bool removeBook(int book_id);
   int insertBook(const Book &book, int user_id);
   vector<Book> getAllUserBooks(int user_id);
   // Authors
   vector<Author> getAllAuthors(void);
   optional<Author> getAuthorById(int author_id);
   bool updateAuthor(const Author &author);
   bool removeAuthor(int author_id);
   int insertAuthor(const Author &author);
   // users
      optional<User> getUserAuth(string & username, string & password);
      // links
      vector<Author> getAllBookAuthors(int student_id);
      bool insertBookAuthor(int student_id, int course_id);
      bool removeBookAuthor(int student_id, int course_id);
};
