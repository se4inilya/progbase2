#ifndef INSERTDIALOG_H
#define INSERTDIALOG_H

#include <QDialog>
#include "storage.h"
#include <QListWidget>

namespace Ui {
class InsertDialog;
}

class InsertDialog : public QDialog
{
    Q_OBJECT

public:
    explicit InsertDialog(QWidget *parent = 0);
    ~InsertDialog();
signals:

    void sendupdatedAuthorList(Storage *);
private slots:
    void on_listWidget_itemDoubleClicked(QListWidgetItem *item);

    void receiveStorage(Storage *, Book *);
private:
    Ui::InsertDialog *ui;

    Storage * storage_;
    Book * book_;
};

#endif // INSERTDIALOG_H
