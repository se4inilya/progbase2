#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include <unistd.h>
#include <getopt.h>

struct __List
{
    int capacity;
    int size;
    void **items;
};

typedef struct __List List;

List *List_alloc();         //
void List_free(List *self); //

void List_init(List *self);   //
void List_deinit(List *self); //

void List_memAlloc(List *self);
void *StrOnHeap(void *str);
void List_cleanHeap(List *self);                           //
void printStrings(List *self, int ind);               //
int List_indexOf(List *self, void *value);            //
bool List_contains(List *self, void *value);          //
bool List_isEmpty(List *self);                        //
void List_print(List *self);                          //
void *List_get(List *self, int index);                //
void List_set(List *self, int index, void *value);    //
size_t List_size(List *self);                         //
void List_insert(List *self, int index, void *value); //
void List_removeAt(List *self, int index);            //
void List_removeAtWithFree(List *self, int index);

void List_add(List *self, void *value);    //
void List_remove(List *self, void *value); //
