#include "csv.h"

int main(int argc, char *argv[])
{
    char data_name[20];
    data_name[0] = '\0';
    char out[20];
    out[0] = '\0';
    int n = 0;
    char str[1000];
    str[0] = '\0';
    bool iflag = false; 
    bool oflag = false;

    int opt;
    while ((opt = getopt(argc, argv, "n:o:")) != -1)
    {
        switch (opt)
        {
        case 'n':
            n = atoi(optarg);
            break;
        case 'o':
            printf("option: %c, value: %s\n", opt, optarg);
            strcpy(out, optarg);
            break;
        case ':':
            printf("option needs a value\n");
            break;
        case '?':
            printf("unknown option: %c\n", optopt);
            break;
        }
    }

    if (optind < argc)
        strcpy(data_name, argv[optind]);

    for (; optind < argc; optind++)
    {
        printf("extra arguments: %s\n", argv[optind]);
    }

    FILE *p = NULL;
    if (strlen(data_name) > 0)
    {
        p = fopen(data_name, "r"); 
        iflag = true;
        if (p == NULL)
        {
            printf("Cannot open \"%s\"\n", data_name);
            exit(EXIT_FAILURE);
        }
        char c = fgetc(p);
        int ind = 0;

        while (c != EOF)
        {
            str[ind++] = c;
            c = fgetc(p);
        }
        fclose(p);
        str[ind] = '\0';
    }

    FILE *pout = NULL;

    if (strlen(out) > 0)
    {
        pout = freopen(out, "w", stdout);
        oflag = true;
        if (pout == NULL)
        {
            printf("Can not open file %s\n", out);
            exit(EXIT_FAILURE);
        }
    }
    
    List *book_list = NULL;
    if (iflag == true)
    {
        List *table = List_alloc();

        Csv_fillTableFromString(table, str);

        book_list = createBooksListFromTable(table);
        if (n > 0)
        {
            for (int i = 0; i < List_size(book_list); i++)
            {
                Book *tmp = List_get(book_list, i);
                int k = tmp->character_capacity;
                if (k >= n)
                {
                    List_removeAtWithFree(book_list, i);
                    i--;
                }
            }
        }
        List *table2 = List_alloc();
        
        if (oflag == 1)
        {
            convertListToCsv(book_list, table2);
            Csv_printTableToFile(table2);
        }
        else
        {
            print_books_array(book_list, 0);
        }

        List_cleanHeap(book_list);
        List_free(book_list);
        Csv_clearTable(table2);
        Csv_clearTable(table);
    }
    else
    {
        Book book1 = {1, "Three musketeers", "Alexander Duma", 52};
        Book book2 = {2, "Hound of the Baskervilles", "Arthur Conan Doyle", 10};
        Book book3 = {3, "Red and black", "Stendall", 7};
        Book book4 = {4, "Hobbit", "John Tolkien", 67};
        Book book5 = {5, "1984", "George Orwell", 20};

        List *table1 = List_alloc();
        List_add(table1, &book1);
        List_add(table1, &book2);
        List_add(table1, &book3);
        List_add(table1, &book4);
        List_add(table1, &book5);

        if (n > 0)
        {
            for (int i = 0; i < List_size(table1); i++)
            {

                Book *book = List_get(table1, i);

                if (book->character_capacity >= n)
                {

                    List_removeAt(table1, i);
                    i--;
                }
            }
        }
        print_books_array(table1, oflag);

        List_free(table1);
    }
    if (strlen(out) > 0)
        fclose(pout);

    return 0;
}





