#include <stdlib.h>
#include <progbase.h>
#include <assert.h>
#include <time.h>
#include <ctype.h>
#include <string.h>

#include "list.h"
#include "deque.h"

void fromListToDeque(List *list, Deque *deq1, Deque *deq2)
{
    for (int i = 0; i < List_size(list); i++)
    {
        if (i % 2 != 0)
        {
            Deque_pushFront(deq1, List_get(list, i));
        }
        else
        {
            Deque_pushBack(deq2, List_get(list, i));
        }
    }
}

void fromDequeToList(List *list, Deque *deq1, Deque *deq2)
{
    while (Deque_size(deq1) != 0)
    {
        List_add(list, Deque_getFromEnd(deq1));
        Deque_popBack(deq1);
    }
    while (Deque_size(deq2) != 0)
    {
        List_add(list, Deque_getFromFront(deq2));
        Deque_popFront(deq2);
    }
}

int main()
{
    FILE *fp;
    fp = fopen("data.txt", "r");
    if (fp == NULL)
    {
        fprintf(stderr, "Error opening file input.txt\n");
        return 1;
    }
    List list;
    List_init(&list);
    List_fillFromString(&list, fp);
    List_print(&list);
    puts("");
    List_negativeNumbersOnFirstPos(&list);
    List_print(&list);
    puts("");

    Deque deq1;
    Deque_init(&deq1);
    Deque deq2;
    Deque_init(&deq2);
    fromListToDeque(&list, &deq1, &deq2);
    Deque_print(&deq1);
    puts("");

    Deque_print(&deq2);
    puts("");

    List list1;
    List_init(&list1);

    fromDequeToList(&list1, &deq1, &deq2);
    List_print(&list1);

    Deque_deinit(&deq1);
    Deque_deinit(&deq2);

    List_deinit(&list);
    List_deinit(&list1);

    fclose(fp);

    return 0;
}
