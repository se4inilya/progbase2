struct Deque
{
    float *items;
    int first;
    int last;
    int capacity;
    int size;
};

typedef struct Deque Deque;

void Deque_init(Deque *self);

void Deque_pushFront( Deque *self, float value);
void Deque_pushBack( Deque *self, float value);
void Deque_popFront( Deque *self);
void Deque_popBack( Deque *self);
void Deque_print( Deque *self);
void Deque_deinit( Deque *self);

void Deque_move( Deque *self, int x);
float Deque_getFromEnd(Deque *deq);
float Deque_getFromFront(Deque *deq);
int Deque_size(Deque *deq);