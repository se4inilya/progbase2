#include <stdio.h>

struct __List
{
    int cap;
    int length;
    float *arr;
};

typedef struct __List List;

void List_init(List *darray); 
void List_deinit(List *darray);
void List_add(List *darray, float newValue);
void List_print(const List *darray);
void List_fillFromString(List *darray, FILE * fp);
void List_negativeNumbersOnFirstPos(List *darray);
int List_size(List *list);
float List_get(List *list, int index);