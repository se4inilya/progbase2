#include <ctype.h>
#include <string.h>
#include <stdlib.h>

#include "list.h"

void List_init(List *darray)
{
    darray->cap = 4;
    darray->arr = malloc(sizeof(float) * darray->cap);
    if (darray == NULL)
    {
        fprintf(stderr, "Allocating memory error\n");
        abort();
    }
    darray->length = 0;
}
void List_deinit(List *darray)
{
    free(darray->arr);
}

void List_add(List *darray, float newValue)
{
    darray->arr[darray->length] = newValue;
    darray->length += 1;
    // realloc mem
    if (darray->length == darray->cap)
    {
        int newcap = darray->cap * 2;
        float *newarr = realloc(darray->arr, sizeof(float) * newcap);
        if (newarr == NULL)
        {
            free(darray->arr);
            fprintf(stderr, "Reallocating memory error\n");
            abort();
        }
        darray->arr = newarr;
        darray->cap = newcap;
    }
}
void List_print(const List *darray)
{
    for (int j = 0; j < darray->length; j++)
    {
        printf("%.3f ", darray->arr[j]);
    }
    printf("\n");
}
void List_fillFromString(List *darray, FILE *fp)
{
    char p[1000];
    fgets(p, 1000, fp);
    // for (int j = 0; p[j] != '\0'; j++)
    // {
    //     printf("%c", p[j]);
    // }
    // printf("\n");
    char p1[1000];
    int i = 0;
    int k = 0;
    int l = 0;
    do
    {

        if (p[k] == ' ' || p[k + 1] == '\0')
        {
            p1[i] = '\0';
            List_add(darray, atof(p1));
            i = 0;
            k++;
        }
        else
        {
            p1[i] = p[k];
            k++;
            i++;
        }
    } while (p[k] != '\0');
}

void List_negativeNumbersOnFirstPos(List *darray)
{
    int counter = 0;
    float tmp = 0;
    for (int i = 0; i < darray->length; i++)
    {
        if (darray->arr[i] < 0)
        {
            tmp = darray->arr[i];
            darray->arr[i] = darray->arr[counter];
            darray->arr[counter] = tmp;
            counter++;
        }
    }
}

int List_size(List *list)
{
    return list->length;
}

float List_get(List *list, int index)
{
    return list->arr[index];
}