#include <stdio.h>
#include <stdlib.h>

#include "deque.h"

void Deque_init(Deque *self)
{
    // Deque *self = NULL;
    // self = malloc(sizeof(struct Deque));
    self->capacity = 4;
    self->first = 0;
    self->last = 0;
    self->size = 0;
    self->items = malloc(sizeof(float) * self->capacity);
    if (self == NULL)
    {
        printf("Error. Can not allocate\n");
        exit(EXIT_FAILURE);
    }
}

void Deque_pushFront(Deque *self, float value)
{
    if (self->size == 0)
    {
        self->items[self->first] = value;
    }
    else
    {
        if (self->last + 1 == self->capacity)
        {
            self->capacity *= 2;
            float *newitems = realloc(self->items, sizeof(float) * self->capacity);
            if (newitems == NULL)
            {
                printf("\nReallocating error\n");
                free(self->items);
                free(self);
                exit(EXIT_FAILURE);
            }
            self->items = newitems;
        }
        Deque_move(self, 1);
        self->items[self->first] = value;
    }
    self->size++;
}
void Deque_pushBack(Deque *self, float value)
{
    if (self->size == 0)
    {
        self->items[self->first] = value;
    }
    else
    {
        if (self->last + 1 == self->capacity)
        {
            self->capacity *= 2;
            float *newitems = realloc(self->items, sizeof(float) * self->capacity);
            if (newitems == NULL)
            {
                printf("\nReallocating error\n");
                free(self->items);
                free(self);
                exit(EXIT_FAILURE);
            }
            self->items = newitems;
        }
        self->last++;
        self->items[self->last] = value;
    }
    self->size++;
}
void Deque_popFront(Deque *self)
{
    if (self->size == 0)
    {
        return;
    }
    Deque_move(self, -1);
    self->size--;
}
void Deque_popBack(Deque *self)
{
    if (self->size == 0)
    {
        return;
    }
    self->last--;
    self->size--;
}
void Deque_print(Deque *self)
{
    for (int i = 0; i <= self->last; i++)
    {
        printf("%.3f ", self->items[i]);
    }
    printf("\n");
}

void Deque_deinit(Deque *self)
{
    free(self->items);
    // free(self);
}

void Deque_move(Deque *self, int x)
{
    if (self->size == 0)
    {
        return;
    }
    if (x < 0)
    {
        for (int i = 1; i <= self->last; i++)
        {
            self->items[i + x] = self->items[i];
        }
        self->last--;
        return;
    }
    if (self->last + x >= self->capacity)
    {
        return;
    }
    for (int i = self->last; i >= 0; i--)
    {
        self->items[i + x] = self->items[i];
    }
    self->last = self->last + x;
}

int Deque_size(Deque *deq){
    return deq->size;
}

float Deque_getFromFront(Deque *deq){
    return deq->items[deq->first];
}

float Deque_getFromEnd(Deque *deq){
    return deq->items[deq->last];
}