#pragma once

#include <string>
#include <vector>

using namespace std;

namespace Sr2
{
    
    enum NumberBase
    {
        Decimal,  // base 10
        Octal,    // base 8
        Hex,  // base 16
    };

    struct Number
    {
        string number;
        NumberBase base;

        Number();
        Number(const string & number);
        Number(const string & number, NumberBase base);
    };
    
    vector<Number> getNumbers(const string & text);
}
