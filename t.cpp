#include <iostream>
#include <string>

using namespace std;

class Human
{
  
    int age;
    int weight;
    string name;
public:
    void takeParametrs(int index)
    {
        cout << "Write age of " << index << " human"<<endl;
        cin >> age;
        cout << "Write weight " << index << " human"<<endl;
        cin >> weight;
        cout << "Write name " << index << " human"<<endl;
        cin >> name;
    }
    void print(int index)
    {
        cout << "Data of " << index << " human :" << endl;
        cout << "Age = " << age << endl;
        cout << "Weight = " << weight << endl;
        cout << "Name is " << name << endl;
    }
};


int main()
{
    Human people[3];
    for (int i = 0; i < 3; i++)
    {
        people[i].takeParametrs(i + 1);
    }
    for (int i = 0; i < 3; i++)
    {
        people[i].print(i);
    }

    return 0;
}
