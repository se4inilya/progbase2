#include "pr5.h"

struct __List
{
    int capasity;
    int length;
    int *item;
};

typedef struct __List List;

List *List_alloc(void)
{
    List *self = malloc(sizeof(List));
    if (self == NULL)
    {
        fprintf(stderr, "Out of memory self\n");
        abort();
    }
    self->capasity = 16;
    self->length = 0;
    self->item = malloc(self->capasity * sizeof(int));

    if (self->item == NULL)
    {
        fprintf(stderr, "Out of memory item\n");
        abort();
    }

    return self;
}
void List_free(List *self)
{

    List_clear(self);
    free(self);
}

size_t List_size(List *self)
{
    return self->length;
}
int List_get(List *self, int index)
{
    return self->item[index];
}
void List_set(List *self, int index, int value)
{
    if (index > self->length)
    {
        fprintf(stderr, "Invalid index\n");
        abort();
    }
    void *temp = &self->item[index];
    self->item[index] = value;
}
void List_insert(List *self, int index, int value)
{
    if (index > self->length)
    {
        fprintf(stderr, "Invalid index\n");
        abort();
    }
    if (self->length == self->capasity)
    {
        self->capasity *= 2;
        self->item = realloc(self->item, self->capasity * sizeof(int));

        if (self->item == NULL)
        {
            fprintf(stderr, "Out of memory item\n");
            abort();
        }
    }
    int count = List_size(self);
    if (count > 0)
        for (int i = count; i > index; i--)
        {
            self->item[i] = self->item[i - 1];
        }
    self->item[index] = value;
    self->length++;
}
void List_removeAt(List *self, int index)
{
    if (index > self->length)
    {
        fprintf(stderr, "Invalid index\n");
        abort();
    }
    void *temp = &self->item[index];
    for (int i = index + 1; i < self->length; i++)
    {
        self->item[i - 1] = self->item[i];
    }
    self->length--;

    if (self->length <= self->capasity / 3 && self->capasity > 16)
    {
        self->capasity /= 2;
        self->item = realloc(self->item, self->capasity * sizeof(int));
    }
}

void List_add(List *self, int value)
{
    if (self->length == self->capasity)
    {
        self->capasity *= 2;
        self->item = realloc(self->item, self->capasity * sizeof(int));

        if (self->item == NULL)
        {
            fprintf(stderr, "Out of memory item\n");
            abort();
        }
    }

    self->item[self->length] = value;
    self->length++;
    Insertion_sort(self);
}
void List_remove(List *self, int value)
{
    int k = 0;
    k = List_indexOf(self, value);
    if (k != -1)
    {
        List_removeAt(self, k);
    }
}
int List_indexOf(List *self, int value)
{
    int first = 0;
    int last = self->length - 1;
    int middle = (first + last) / 2;

    while (first <= last)
    {
        if (self->item[middle] < value)
            first = middle + 1;
        else if (self->item[middle] == value)
        {
            return middle;
        }
        else
            last = middle - 1;

        middle = (first + last) / 2;
    }
    return -1;
}
bool List_contains(List *self, int value)
{
    int k = 0;
    k = List_indexOf(self, value);
    if (k != -1)
    {
        return true;
    }
    return false;
}
bool List_isEmpty(List *self)
{
    if (self->length == 0)
        return true;
    else
        return false;
}
void List_clear(List *self)
{
    free(self->item);
}

void Insertion_sort(List *self)
{
    int c = 0;
    int d = 0;
    int t = 0;
    for (c = 1; c < self->length; c++)
    {
        d = c;

        while (d > 0 && self->item[d - 1] > self->item[d])
        {
            t = self->item[d];
            self->item[d] = self->item[d - 1];
            self->item[d - 1] = t;

            d--;
        }
    }
}

void List_print(List *self)
{
    for (int i = 0; i < self->length; i++)
    {
        printf("%i ", self->item[i]);
    }
    printf("\n");
}