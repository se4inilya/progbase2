#include <iostream>
#include <string>

using namespace std;

class Point
{
    int x, y;

  public:
    int GetX()
    {
        return x;
    }
    void setX(int valueX)
    {
        x = valueX;
    }
    int GetY()
    {
        return y;
    }
    void setY(int valueY)
    {
        y = valueY;
    }
    void print()
    {
        cout << "x = " << x << endl
             << "y = " << y << endl;
    }
};

class CoffeeGrinder
{
  private:
    int voltage;
    bool checkVoltage()
    {
        if (voltage < 220)
            return false;
    }

  public:
    void set(int value)
    {
        voltage = value;
    }
    void startOfMachine()
    {
        if (checkVoltage())
        {
            cout << "Coffee" << endl;
        }
        else
        {
            cout << "error" << endl;
        }
    }
};

int main()
{
    // Point point;
    // point.setX(20);
    // point.setY(30);
    // point.print();
    // int result = point.GetX() + point.GetY();
    // cout<<result<<endl;

    CoffeeGrinder cm;
    cm.set(110);
    cm.startOfMachine();

    return 0;
}