#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

typedef struct __List List;

List *  List_alloc    (void);
void    List_free     (List * self);

size_t  List_size     (List * self);
int      List_get      (List * self, int index);
void    List_set      (List * self, int index, int value);
void    List_insert   (List * self, int index, int value);
void    List_removeAt (List * self, int index);

void    List_add      (List * self, int value);
void    List_remove   (List * self, int value);
int     List_indexOf  (List * self, int value);
bool    List_contains (List * self, int value);
bool    List_isEmpty  (List * self);
void    List_clear    (List * self);

void Insertion_sort(List *self);

void List_print(List *self);
